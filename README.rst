=====================
Installing Arch Linux
=====================
Initial preparation
===================

.. code:: shell-session

    # iwctl  # Connect to Wi-Fi.
    [iwd]# device list
    [iwd]# station ${wifi_device} connect ${ssid}
    [iwd]# exit
    # timedatectl set-ntp true

Creating file systems and mounting them
=======================================

Table 1.  Disk ``/dev/sda`` partition table.

+----+---------------+----------------------------------------------------------------------------+
| \# |       MP      |                                    Note                                    |
+====+===============+============================================================================+
| 1  | ``/mnt/boot`` | If non-UEFI mark as boot, otherwise set type ``EFI System``                |
+----+---------------+----------------------------------------------------------------------------+
| 2  | ``/mnt``      | If non-UEFI set type ``Linux``, otherwise set type ``Linux root (x86-64)`` |
+----+---------------+----------------------------------------------------------------------------+

.. code:: shell-session

    # fdisk /dev/sda  # Create partition table.
    # mkfs.fat -F 32 /dev/sda1
    # mkfs.ext4 -L archlinux /dev/sda2
    # mount /dev/sda2 /mnt
    # mkdir /mnt/boot && mount /dev/sda1 /mnt/boot

Basic installation and setup
============================

Install ``grub`` for non-UEFI.  See `“Microcode”`_, `fstab example <fstab>`_.

.. code:: shell-session

    # pacstrap /mnt base linux{,-firmware} e2fsprogs dosfstools kakoune
    # genfstab -U /mnt >> /mnt/etc/fstab  # Check `/mnt/etc/fstab`.
    # arch-chroot /mnt
    # ln -sf "/usr/share/zoneinfo/${region}/${city}" /etc/localtime
    # hwclock --systohc  # Clock must be UTC (otherwise use `-l` option).
    # kak /etc/locale.gen  # Uncomment `#en_US.UTF-8 UTF-8`.
    # locale-gen
    # echo LANG=en_US.UTF-8 > /etc/locale.conf
    # echo ${hostname} > /etc/hostname
    # cat > /etc/hosts << EOF
    > 127.0.0.1   localhost
    > ::1         localhost
    > 127.0.1.1   ${hostname}
    > EOF
    # # Uncomment `#Color`, `#ParallelDownloads`, multilib section.
    # kak /etc/pacman.conf
    # pacman-key --init  # Just in case.
    # pacman -Syu
    # pacman -Sw iwd  # For Wi-Fi.
    # passwd

Boot loader installation
========================

For non-UEFI:

.. code:: shell-session

    # grub-install /dev/sda
    # grub-mkconfig -o /boot/grub/grub.cfg

For UEFI:

.. code:: shell-session

    # bootctl install
    # cat > /boot/loader/entries/archlinux.conf << EOF
    > title Arch Linux
    > linux /vmlinuz-linux
    > initrd /${microcode_img}
    > initrd /initramfs-linux.img
    > options root="UUID=${root_uuid}" rw quiet splash
    > EOF
    # cp /boot/loader/entries/archlinux{,-fallback-initramfs}.conf
    # sed -i '/\/initramfs-linux.img$/s/\./-fallback./g' \
    >   /boot/loader/entries/archlinux-fallback-initramfs.conf

Setup
=====

.. code:: shell-session

    # exit
    # umount -R /mnt && reboot
    # pacman -S iwd  # For Wi-Fi.
    # systemctl enable --now iwd fstrim.timer \
    >   systemd-{boot-update,networkd,resolved,timesyncd}
    # iwctl
    [iwd]# device list
    [iwd]# station ${wifi_device} connect ${ssid}
    [iwd]# exit
    # cat > /etc/systemd/network/25-wireless.network << EOF
    > [Match]
    > Name=${wifi_device}
    >
    > [Network]
    > DHCP=yes
    > IPv6PrivacyExtensions=yes
    > IgnoreCarrierLoss=3s
    >
    > [DHCPv4]
    > RouteMetric=600
    >
    > [IPv6AcceptRA]
    > RouteMetric=600
    > EOF
    # kak /etc/systemd/timesyncd.conf  # Add `time.cloudflare.com` to NTP.
    # reboot

See `“Hardware video acceleration”`_, `“Xorg, Driver installation”`_,
`“AppArmor, Installation”`_, `“VirtualBox, Install the core packages”`_.
Install ``blueman`` for Bluetooth.

.. code:: shell-session

    # timedatectl set-ntp true
    # pacman -Syu --needed noto-fonts{,-cjk,-emoji} ttf-jetbrains-mono bspwm
    >   sxhkd alacritty kitty breeze-{gtk,icons} clipmenu dunst feh flameshot \
    >   lightdm{,-gtk-greeter} picom playerctl polkit-gnome rofi wmname \
    >   xorg-{server,xinput,xrdb} xsecurelock
    # pacman -Syu --needed zsh{,-completions,-theme-powerlevel10k} base-devel \
    >   apparmor pulseaudio{,-bluetooth} youtube-dl wget git{,-delta}
    >   android-{tools,udev,file-transfer} systemd-swap cups xdg-user-dirs \
    >   openssh reflector pass pwgen pacman-contrib transmission-gtk \
    >   thunderbird vlc gimp krita qalculate-gtk telegram-desktop \
    >   libreoffice-still{,-ru} asp translate-shell discord ipython \
    >   torbrowser-launcher docker wireshark-qt qutebrowser minted chezmoi \
    >   texlive-{bin,langcyrillic,latexextra,science} \
    >   {bash,haskell}-language-server clang code cpupower fzf github-cli \
    >   gnome-keyring hdparm htop jq kak-lsp man-{db,pages} texinfo texlab mypy \
    >   neovim obs-studio pacman-contrib pycharm-community-edition ranger \
    >   reuse ripgrep rstcheck steam strace time udisks2 \
    >   virtualbox{,-guest-iso} vscode-{css,html,json}-languageserver xclip \
    >   zathura{,-pdf-mupdf} python-{pip,pylint,black,isort,neovim,lsp-server} \
    >   firefox{,-{clearurls,dark-reader,extension-https-everywhere,i18n-en-us,tridactyl,ublock-origin}}
    # pacman -Syu --needed --asdeps tk  # For gitk.
    # sed '/^#greeter-session=example-gtk-gnome$/s/example-gtk-gnome/lightdm-gtk-greeter/g' \
    >   -i /etc/lightdm/lightdm.conf
    # kak /etc/lightdm/lightdm-gtk-greeter.conf  # Customize.
    # kak /etc/makepkg.conf  # Customize.
    # kak /etc/systemd/swap.conf  # Set `swapfc_enabled=1`.
    # systemctl enable apparmor bluetooth cups docker lightdm systemd-swap
    # useradd -G wheel,wireshark,adbusers,users -s "`which zsh`" -m \
    >   ${username}
    # passwd ${username}
    # sed -i '/^# %wheel ALL=(ALL) ALL$/s/^# //g' /etc/sudoers
    # sudo -iu ${username}
    $ xdg-user-dirs-update
    $ sed -i '/^XDG/s|/[[:upper:]]|\L&|g' ~/.config/user-dirs.dirs
    $ for i in Desktop Downloads Templates Public Documents Music Pictures \
    >     Videos; do
    >   mv ~/$i ~/${i@L}
    > done
    $ git clone https://aur.archlinux.org/yay.git
    $ cd yay && makepkg -si && cd - && rm -rf yay
    $ yay -S polybar mindforger zeal ventoy-bin
    $ systemctl --user enable clipmenud
    $ chezmoi init k4leg --apply
    $ reboot

.. _“Microcode”: https://wiki.archlinux.org/title/Microcode
.. _“Hardware video acceleration”: https://wiki.archlinux.org/title/Hardware_video_acceleration
.. _“Xorg, Driver installation”: https://wiki.archlinux.org/title/Xorg#Driver_installation
.. _“AppArmor, Installation”: https://wiki.archlinux.org/title/AppArmor#Installation
.. _“VirtualBox, Install the core packages”: https://wiki.archlinux.org/title/VirtualBox#Install_the_core_packages
